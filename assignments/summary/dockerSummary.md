# NEED of a DOCKER

It becomes hard to manage dependencies as we build large projects and if we want to deploy them in various environments. So we will be using docker to deploy our product.

## DOCKER TERMS You need to know

#### 1. Docker : 
    Docker is a program for developers to develop, and run applications with containers.

#### 2. Docker Image:
    a) A Docker image is contains everything needed to run an applications as a container. This includes:
        * code
        * runtime
        * libraries
        * environment variables
        * configuration files
    b) The image can then be deployed to any Docker environment and as a container.

#### 3. Container : 
    a) A Docker container is a running Docker image.
    b) From one image you can create multiple containers .

#### 4. Docker Hub : 
    Docker Hub is like GitHub but for docker images and containers.

#### 5. Installation Process

** Step-1 : Uninstall the older version of docker if is already installed **

    $ sudo apt-get remove docker docker-engine docker.io containerd runc

** Step-2 : Installing CE (Community Docker Engine) **

    $ sudo apt-get update
    $ sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common
    $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    $ sudo apt-key fingerprint 0EBFCD88
    $ sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable nightly test"
    $ sudo apt-get update
    $ sudo apt-get install docker-ce docker-ce-cli containerd.io

    // Check if docker is successfully installed in your system
    $ sudo docker run hello-world
    
## Docker basic commands
    1. docker ps
    2. docker start
    3. docker stop
    4. docker run
    5. docker rm

## Common Operations on Dockers
    1. Download/pull the docker images that you want to work with.
    2. Copy your code inside the docker
    3. Access docker terminal
    4. Install and additional required dependencies
    5. Compile and Run the Code inside docker
    6. Document steps to run your program in README.md file
    7. Commit the changes done to the docker.
    8. Push docker image to the docker-hub and share repository with people who want to try your code


